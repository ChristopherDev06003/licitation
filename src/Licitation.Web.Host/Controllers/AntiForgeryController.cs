using Microsoft.AspNetCore.Antiforgery;
using Licitation.Controllers;

namespace Licitation.Web.Host.Controllers
{
    public class AntiForgeryController : LicitationControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
