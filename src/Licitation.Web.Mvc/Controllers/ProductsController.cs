﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Licitation.Controllers;
using Licitation.Models.Dto;
using Licitation.Products;
using Licitation.Products.Dto;
using Licitation.Web.Models.Products;
using Microsoft.AspNetCore.Mvc;

namespace Licitation.Web.Mvc.Controllers
{
    public class ProductsController : LicitationControllerBase
    {
        private readonly IProductsAppService _productsAppService;

        public ProductsController(IProductsAppService productsAppService)
        {
            _productsAppService = productsAppService;
        }

        public async Task<IActionResult> Index()
        {
            var list = await _productsAppService.GetAll();
            var model = new ProductsViewModel
            {
                ListProducts = list
            };

            return View(model);
        }

        public async Task<ActionResult> EditModal(int id)
        {
            var prod = await _productsAppService.GetById(id);
            var model = new CreateOrEditProductsModalViewModel
            {
                Product = prod
            };
            return View("_EditModal", model);
        }
    }
}