﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using Licitation.Controllers;

namespace Licitation.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : LicitationControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
