﻿using Licitation.Products.Dto;

namespace Licitation.Web.Mvc.Controllers
{
    internal class CreateOrEditProductsViewModel
    {
        public ProductsDto Products { get; set; } 
    }
}