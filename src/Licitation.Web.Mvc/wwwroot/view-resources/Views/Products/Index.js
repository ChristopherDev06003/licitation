﻿(function () {
    $(function () {

        var _productsService = abp.services.app.products;
        var _$modal = $('#prodCreateModal');
        var _$form = _$modal.find('form');


        _$form.find('button[type="submit"]').click(async function (e) {
            e.preventDefault();

            var name = $("#Name").val();
            var brand = $("#Brand").val();
            var purchasePrice = $("#PurchasePrice").val();
            var provider = $("#Provider").val();

            var input = await _productsService.convertToCreate(name, brand, purchasePrice, provider);

            abp.ui.setBusy(_$modal);
            _productsService.createOrEdit(input).done(function () {
                _$modal.modal('hide');
                location.reload(true); //reload page to see new user!
            }).always(function () {
                abp.ui.clearBusy(_$modal);
            });
        });

        $('#RefreshButton').click(function () {
            refreshProdList();
        });

        $('.delete-prod').click(function () {
            var userId = $(this).attr("data-prod-id");
            var userName = $(this).attr('data-prod-name');

            deleteProd(userId, userName);
        });

        $('.edit-prod').click(function (e) {
            var prodId = $(this).attr("data-prod-id");

            e.preventDefault();
            abp.ajax({
                url: abp.appPath + 'Products/EditModal?id=' + prodId,
                type: 'POST',
                dataType: 'html',
                success: function (content) {
                    $('#prodEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });

        function refreshProdList() {
            location.reload(true); //reload page to see new user!
        }

        function deleteProd(prodId, prodName) {
            abp.message.confirm(
                abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'Licitation'), prodName),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _productsService.delete({
                            id: prodId
                        }).done(function () {
                            refreshProdList();
                        });
                    }
                }
            );
        }
    });
})();
