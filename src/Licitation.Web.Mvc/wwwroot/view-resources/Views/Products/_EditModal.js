﻿(function ($) {

    var _productsService = abp.services.app.products;
    var _$modal = $('#prodEditModal');
    var _$form = $('form[name=EditForm]');

    async function save() {

        var id = $("#EditId").val();
        var name = $("#EditName").val();
        var brand = $("#EditBrand").val();
        var purchasePrice = $("#EditPurchasePrice").val();
        var provider = $("#EditProvider").val();

        var input = await _productsService.convertToCreate(name, brand, purchasePrice, provider,id);

        abp.ui.setBusy(_$form);
        _productsService.createOrEdit(input).done(function () {
            _$modal.modal('hide');
            location.reload(true); 
        }).always(function () {
            abp.ui.clearBusy(_$modal);
        });
    }

    //Handle save button click
    _$form.closest('div.modal-content').find(".save-button").click(function (e) {
        e.preventDefault();
        save();
    });

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    $.AdminBSB.input.activate(_$form);

    _$modal.on('shown.bs.modal', function () {
        _$form.find('input[type=text]:first').focus();
    });
})(jQuery);