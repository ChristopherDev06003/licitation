﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace Licitation.Web.Views
{
    public abstract class LicitationRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected LicitationRazorPage()
        {
            LocalizationSourceName = LicitationConsts.LocalizationSourceName;
        }
    }
}
