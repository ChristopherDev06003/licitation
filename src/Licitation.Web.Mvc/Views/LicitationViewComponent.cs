﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace Licitation.Web.Views
{
    public abstract class LicitationViewComponent : AbpViewComponent
    {
        protected LicitationViewComponent()
        {
            LocalizationSourceName = LicitationConsts.LocalizationSourceName;
        }
    }
}
