using System.Collections.Generic;
using Licitation.Roles.Dto;
using Licitation.Users.Dto;

namespace Licitation.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
