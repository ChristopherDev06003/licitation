﻿using System.Collections.Generic;
using Licitation.Roles.Dto;

namespace Licitation.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}