using Licitation.Products.Dto;
using System.Collections.Generic;

namespace Licitation.Web.Models.Products
{
    public class ProductsViewModel
    {
		public List<ProductsDto> ListProducts { get; set; }
    }
}