
using Abp.Extensions;
using Licitation.Products.Dto;

namespace Licitation.Web.Models.Products
{
    public class CreateOrEditProductsModalViewModel
    {
       public ProductsDto Product { get; set; }
    }
}