﻿namespace Licitation
{
    public class LicitationConsts
    {
        public const string LocalizationSourceName = "Licitation";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
