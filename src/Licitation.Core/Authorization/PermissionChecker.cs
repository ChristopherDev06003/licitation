﻿using Abp.Authorization;
using Licitation.Authorization.Roles;
using Licitation.Authorization.Users;

namespace Licitation.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
