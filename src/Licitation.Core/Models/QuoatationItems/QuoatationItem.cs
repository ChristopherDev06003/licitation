﻿using Abp.Domain.Entities.Auditing;
using Licitation.Models.Products;
using Licitation.Models.Quotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Licitation.Models.QuoatationItems
{
    public class QuoatationItem: FullAuditedEntity
    {
        public Product Product { get; set; }
        public Quotation Quotation { get; set; }
        public int Quantity { get; set; }
        public float MaxPrice { get; set; }
        public float MinPrice { get; set; }
        public float FinalPrice { get; set; }
    }
}
