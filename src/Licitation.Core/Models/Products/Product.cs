﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;

namespace Licitation.Models.Products
{
    public class Product: FullAuditedEntity
    {
        public string Name { get; set; }

        public string Brand { get; set; }

        public string Provider { get; set; }

        public float PurchasePrice { get; set; }
    }
}
