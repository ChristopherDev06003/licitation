﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Licitation.Models
{
    public class Products: FullAuditedEntity
    {
        public string Name { get; set; }

        public string Brand { get; set; }

        public string Provider { get; set; }

        public float PurchasePrice { get; set; }
    }
}
