﻿using Abp.Domain.Entities.Auditing;
using Licitation.Models.Quotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Licitation.Models.Licitacaos
{
    public class Licitacao : FullAuditedEntity
    {
        public string Organ { get; set; }
        public DateTime TradingDay { get; set; }
        public string Model { get; set; }
        public Quotation Quotation { get; set; }
        public string Status { get; set; }
    }
}
