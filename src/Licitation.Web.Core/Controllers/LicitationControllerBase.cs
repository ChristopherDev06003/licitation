using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace Licitation.Controllers
{
    public abstract class LicitationControllerBase: AbpController
    {
        protected LicitationControllerBase()
        {
            LocalizationSourceName = LicitationConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
