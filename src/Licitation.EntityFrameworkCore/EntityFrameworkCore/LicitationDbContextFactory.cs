﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Licitation.Configuration;
using Licitation.Web;

namespace Licitation.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class LicitationDbContextFactory : IDesignTimeDbContextFactory<LicitationDbContext>
    {
        public LicitationDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<LicitationDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            LicitationDbContextConfigurer.Configure(builder, configuration.GetConnectionString(LicitationConsts.ConnectionStringName));

            return new LicitationDbContext(builder.Options);
        }
    }
}
