﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Licitation.Authorization.Roles;
using Licitation.Authorization.Users;
using Licitation.MultiTenancy;
using Licitation.Models.Products;
using Licitation.Models.Quotations;
using Licitation.Models.Licitacaos;
using Licitation.Models.QuoatationItems;

namespace Licitation.EntityFrameworkCore
{
    public class LicitationDbContext : AbpZeroDbContext<Tenant, Role, User, LicitationDbContext>
    {
        /* Define a DbSet for each entity of the applicatioadd-migrationn */
        public DbSet<Product> Products { get; set; }
        public DbSet<Quotation> Quotation { get; set; }
        public DbSet<Licitacao> Licitacao { get; set; }
        public DbSet<QuoatationItem> QuoatationItem { get; set; }

        public LicitationDbContext(DbContextOptions<LicitationDbContext> options)
            : base(options)
        {
        }
    }
}
