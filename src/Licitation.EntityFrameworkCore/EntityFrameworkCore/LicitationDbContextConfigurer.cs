using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Licitation.EntityFrameworkCore
{
    public static class LicitationDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<LicitationDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<LicitationDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
