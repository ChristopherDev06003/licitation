﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using Licitation.Configuration.Dto;

namespace Licitation.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : LicitationAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
