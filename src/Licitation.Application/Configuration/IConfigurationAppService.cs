﻿using System.Threading.Tasks;
using Licitation.Configuration.Dto;

namespace Licitation.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
