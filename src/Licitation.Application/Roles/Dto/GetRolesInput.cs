﻿namespace Licitation.Roles.Dto
{
    public class GetRolesInput
    {
        public string Permission { get; set; }
    }
}
