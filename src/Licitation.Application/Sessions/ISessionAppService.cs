﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Licitation.Sessions.Dto;

namespace Licitation.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
