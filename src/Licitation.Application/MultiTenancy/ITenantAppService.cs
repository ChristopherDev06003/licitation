﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Licitation.MultiTenancy.Dto;

namespace Licitation.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

