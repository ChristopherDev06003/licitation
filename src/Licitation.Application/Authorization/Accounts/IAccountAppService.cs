﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Licitation.Authorization.Accounts.Dto;

namespace Licitation.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
