using System.ComponentModel.DataAnnotations;

namespace Licitation.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}