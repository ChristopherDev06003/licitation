using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Licitation.Roles.Dto;
using Licitation.Users.Dto;

namespace Licitation.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
