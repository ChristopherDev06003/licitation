﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Licitation.Authorization;

namespace Licitation
{
    [DependsOn(
        typeof(LicitationCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class LicitationApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<LicitationAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(LicitationApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
