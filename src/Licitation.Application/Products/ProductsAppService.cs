﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Licitation.Models.Dto;
using Licitation.Models.Products;
using Licitation.Products;
using Licitation.Products.Dto;
using Microsoft.EntityFrameworkCore;

namespace Licitation.Products
{
    public class ProductsAppService :  LicitationAppServiceBase, IProductsAppService
    {
        private readonly IRepository<Product> _productsRepository;

        public ProductsAppService(IRepository<Product> productsRepository)
        {
            _productsRepository = productsRepository;
        }

        public async Task<List<ProductsDto>> GetAll()
        {
            var filteredprod = await _productsRepository.GetAll().OrderBy(d=> d.Name).ToListAsync();
            var output = new List<ProductsDto>();
            foreach (var item in filteredprod)
            {
                var dto = new ProductsDto()
                { 
                    Id = item.Id,
                    Name = item.Name,
                    Brand = item.Brand,
                    PurchasePrice = item.PurchasePrice,
                    Provider = item.Provider
                };
                output.Add(dto);
            }
            return output;
        }

        public async Task<ProductsDto> GetById(int id)
        {
            var prod = await _productsRepository.GetAll().Where(d => d.Id == id).FirstOrDefaultAsync();
            var output = new ProductsDto() { Id = prod.Id, Name = prod.Name, Brand = prod.Brand, Provider = prod.Provider, PurchasePrice = prod.PurchasePrice };
            return output;
        }

        public async Task<ProductForViewDto> GetProductForView(int id)
        {
            var prod = await _productsRepository.GetAsync(id);
            var output = new ProductForViewDto { Product = ObjectMapper.Map<ProductsDto>(prod) };
            return output;
        }

        public async Task<GetProductForEditOutput> GetQProductForEdit(EntityDto input)
        {
            var prod = await _productsRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetProductForEditOutput { Product = ObjectMapper.Map<CreateOrEditProductDto>(prod) };
            return output;
        }

        public async Task CreateOrEdit(CreateOrEditProductDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        public CreateOrEditProductDto ConvertToCreate( string name, string brand, float purchasePrice, string provider, int? id)
        {
            if (id.HasValue)
            {
                var output = new CreateOrEditProductDto()
                {
                    Id = id,
                    Name = name,
                    Brand = brand,
                    PurchasePrice = purchasePrice,
                    Provider = provider
                };

                return output;
            }
            else
            {
                var output = new CreateOrEditProductDto()
                {

                    Name = name,
                    Brand = brand,
                    PurchasePrice = purchasePrice,
                    Provider = provider
                };

                return output;
            };
        }



        protected virtual async Task Create(CreateOrEditProductDto input)
        {
            var prod = ObjectMapper.Map<Product>(input);
            await _productsRepository.InsertAsync(prod);
            CurrentUnitOfWork.SaveChanges();
        }

        protected virtual async Task Update(CreateOrEditProductDto input)
        {
            var prod = await _productsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, prod);
        }

        public async Task Delete(EntityDto input)
        {
            await _productsRepository.DeleteAsync(input.Id);
        }
    }
}

