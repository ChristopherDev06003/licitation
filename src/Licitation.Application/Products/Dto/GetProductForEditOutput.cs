﻿using Licitation.Products.Dto;

namespace Licitation.Models.Dto
{
    public class GetProductForEditOutput
    {
        public CreateOrEditProductDto Product { get; set; }
    }
}
