﻿using Licitation.Products.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Licitation.Models.Dto
{
    public class ProductForViewDto
    {
        public ProductsDto Product { get; set; }
    }
}
