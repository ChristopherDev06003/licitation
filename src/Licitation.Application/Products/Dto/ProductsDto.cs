using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Licitation.Authorization.Users;
using Licitation.Models.Products;

namespace Licitation.Products.Dto
{
    [AutoMapTo(typeof(Product))]
    public class ProductsDto : EntityDto
    {
        public string Name { get; set; }

        public string Brand { get; set; }

        public float PurchasePrice { get; set; }

        public string Provider { get; set; }
    }
}
