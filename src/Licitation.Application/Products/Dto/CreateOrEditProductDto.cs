
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Licitation.Models.Products;

namespace Licitation.Products.Dto
{
    [AutoMapTo(typeof(Product))]
    public class CreateOrEditProductDto : EntityDto<int?>
    {
        public string Name { get; set; }

        public string Brand { get; set; }

        public float PurchasePrice { get; set; }

        public string Provider { get; set; }

    }
}
