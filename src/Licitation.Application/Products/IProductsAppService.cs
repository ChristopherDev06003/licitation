using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Licitation.Models.Dto;
using Licitation.Products.Dto;

namespace Licitation.Products
{
    public interface IProductsAppService : IApplicationService
    {
        Task<List<ProductsDto>> GetAll();
        Task<ProductsDto> GetById(int id);
        Task<ProductForViewDto> GetProductForView(int id);
        Task<GetProductForEditOutput> GetQProductForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditProductDto input);
        Task Delete(EntityDto input);
        CreateOrEditProductDto ConvertToCreate(string name, string brand, float purchasePrice, string provider, int? id);
    }
}
