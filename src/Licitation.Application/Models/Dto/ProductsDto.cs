using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Licitation.Authorization.Users;

namespace Licitation.Products.Dto
{
    public class ProductsDto : EntityDto
    {
        public string Name { get; set; }

        public string Brand { get; set; }

        public float PurchasePrice { get; set; }
    }
}
